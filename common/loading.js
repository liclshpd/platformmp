/*显示Loding */
export function showLoading(message) {
	uni.showLoading({
		title: message == null ? '加载中' : message
	});
}


/*关闭Loading */
export function hideLoading() {
	uni.hideLoading();
}