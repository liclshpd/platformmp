
/*顶部弹出 吐司*/
export function toastTop(message) {
	uni.showToast({
		icon: "none",
		title: message,
		position:"top"
	})
}

/*中间弹出  吐司*/
export function toastCenter(message) {
	uni.showToast({
		icon: "none",
		title: message,
		position:"center"
	})
}

/*底部弹出 吐司*/
export function toastBottom(message) {
	uni.showToast({
		icon: "none",
		title: message,
		position:"bottom"
	})
}